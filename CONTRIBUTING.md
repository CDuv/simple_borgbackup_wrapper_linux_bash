# To contribute to the project

## Git commits

Commits in this project follow the [*Conventional Commits* specification v1.0.0](https://www.conventionalcommits.org).

Allowed scopes are:

* *repo*: Changes related to *BorgBackup* repositories.
* *backup*: Changes related to commands for backuping with *BorgBackup* (creating *archives*).
* *restore*: Changes related to commands for restoring with *BorgBackup* (reading *archives*).
* *info*: Changes related to commands for getting informations on *BorgBackup* data sets.
* *clean*: Changes related to commands for cleaning *BorgBackup* data sets.
* *borg*: Changes related to or to cope with any external *BorgBackup* changes.

## Code source

Source code files must:

* Use *Linux type* end of line (*EOL*): `LF`
* Ends by a *newline* character (id. had an empty line at the end).
