Simple *BorgBackup* wrapper for Linux
=====================================

The scripts in this project are simple wrappers around the `borg` command from
[*BorgBackup*](https://www.borgbackup.org). Their goal is to provide a simple
way of using *BorgBackup*.

It eases dealing with repository path and repository's key passphrase.

# Configuration

Configuration parameters are in a `config.sh` file. Scripts will look for
this file in several folders and stop once they find it.
The list of paths to look into is in the `config-paths.txt` file (default is
`./config`). If the `SIMPLEBORGWRAPPER_CONFIGDIR` environment variable is set,
this path will be used instead, no matter what `config-paths.txt` contains.
If the path also contains a `config.local.sh` file it will be loaded, **after**
`config.sh` so the former can override parameters of the latter.

# Usage

```Shell
cat > ./config/config.local.sh <<'EOT'
SOURCE_DIRPATHS=("/home/user" "/var/www")
REPOSITORY_DIRPATH="/var/backups/borg-repo"
EOT

# Create a repository
./borg-init.sh

# Do a backup 
./borg-backup.sh

# List the backup
./borg-list.sh
```

# Supported *BorgBackup* commands

Not all *BorgBackup* commands were wrapped, only the following (and they don't
support all the original options):

* [`init`](https://borgbackup.readthedocs.io/en/stable/usage/init.html) : `borg-init.sh`
* [`create`](https://borgbackup.readthedocs.io/en/stable/usage/create.html) : `borg-backup.sh`
* [`extract`](https://borgbackup.readthedocs.io/en/stable/usage/extract.html) : `borg-restore.sh`
* [`check`](https://borgbackup.readthedocs.io/en/stable/usage/check.html) : `borg-check.sh`
* [`list`](https://borgbackup.readthedocs.io/en/stable/usage/list.html) : `borg-list.sh`
* [`diff`](https://borgbackup.readthedocs.io/en/stable/usage/diff.html) : `borg-diff.sh`
* [`delete`](https://borgbackup.readthedocs.io/en/stable/usage/delete.html) : `borg-delete.sh`
* [`prune`](https://borgbackup.readthedocs.io/en/stable/usage/prune.html) : `borg-prune.sh`
* [`info`](https://borgbackup.readthedocs.io/en/stable/usage/info.html) : `borg-info.sh`
* [`mount`](https://borgbackup.readthedocs.io/en/stable/usage/mount.html) : `borg-mount.sh`
* [`umount`](https://borgbackup.readthedocs.io/en/stable/usage/mount.html#borg-umount) : `borg-umount.sh`
* [`export-tar`](https://borgbackup.readthedocs.io/en/stable/usage/export-tar.html) : `borg-export-tar.sh`
