#!/bin/bash

# Base configuration for BorgBackup Linux wrappers scripts.
#
# Will file contains the basic configuration settings required by the scripts.
#
# @author DUVERGIER Claude

set -euo pipefail

# Path to this directory, so that it can be re-used in configuration parameters
readonly CONFDIR="$(dirname -- "$(realpath --no-symlinks -- "${BASH_SOURCE}")")"


BORG_BIN_FILEPATH="/usr/bin/borg"
BORG_ENCRYPTION="repokey-blake2"
BORG_CACHE_DIRPATH="/tmp/borg-cache"
BORG_TEMP_DIRPATH="/tmp/borg-tmp"

# File containing the repository passphrase.
# Tip: Apply the "u=rw,go=" permissions
REPOSITORIES_PASSWORD_FILEPATH="${CONFDIR}/secret.txt"

# Path(s) to backup
SOURCE_DIRPATHS=("/etc" "/root")

# Path of the BorgBackup repository (id. where to backup)
REPOSITORY_DIRPATH="/tmp/borg-repo"

# Compression level (see `borg help compression` and `--compression` parameter)
ARCHIVE_COMPRESSION="lz4"

# Path to a file containing exclusions (`--exclude-from` parameter)
# See documentation for the content: https://borgbackup.readthedocs.io/en/stable/usage/help.html#borg-help-patterns
#CREATEARCHIVE_EXCLUSION_FILEPATH="/dev/null"
CREATEARCHIVE_EXCLUSION_FILEPATH="${CONFDIR}/exclude.txt"

# The name of the special file that can exclude a whole directory from being
# backuped only by residing in it (`--exclude-if-present` parameter)
CREATEARCHIVE_SPECIAL_FILE_NAME_TO_EXCLUDE_A_DIRECTORY=".skip_backup"

# The default name of archives (can use "{now}", "{utcnow}", "{fqdn}",
# "{hostname}", "{user}" placeholders, see `man borg-placeholders`)
CREATEARCHIVE_DEFAULT_NAME_PREFIX="backup_"

# Extra parameters to pass to `borg create`
CREATEARCHIVE_EXTRA_ARGUMENTS=()
# CREATEARCHIVE_EXTRA_ARGUMENTS=("--dry-run" "--verbose")

PRUNING_EXTRA_ARGUMENTS=(
    # Keep at the last **3** archives
    "--keep-last=3"
    # Keep all from past **1 week**
    "--keep-within=1w"
    # Keep the last archive of each past **4** weeks
    "--keep-weekly=4"
)
