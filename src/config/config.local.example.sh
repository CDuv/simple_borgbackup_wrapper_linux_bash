#!/bin/bash

# Example of an extra/special configuration for BorgBackup Linux wrappers scripts.
#
# Any configuration setting set here will override parameters' value of "config.sh".
#
# @author DUVERGIER Claude

# Path(s) to backup
SOURCE_DIRPATHS=("/home/user" "/var/www")

# Path of the BorgBackup repository (id. where to backup)
REPOSITORY_DIRPATH="/var/backups/borg-repo"
