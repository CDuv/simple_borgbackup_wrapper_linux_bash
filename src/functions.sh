#!/bin/bash

# Generic functions

echo_stderr() {
    local message="${1}"

    (>&2 echo "${message}")
}

echo_error() {
    local message="${1}"

    echo_stderr "Error: ${message}"
}

echo_info() {
    local message="${1}"

    echo_stderr "Info: ${message}"
}

echo_success() {
    local message="${1}"

    echo_stderr "Success: ${message}"
}

echo_error_and_exit() {
    local message="${1}"
    local exit_code

    if [ $# -ge 2 ]; then
        exit_code=${2}
    else
        exit_code=1
    fi

    echo_error "${message}"
    exit ${exit_code}
}

# /Generic functions


# Application functions

##
# Will try to load the configuration file(s).
#
# @return integer   0 if configuration was loaded successfully, 0 otherwise.
##
load_configuration() {
    local paths

    if [ -z "${SIMPLEBORGWRAPPER_CONFIGDIR+not_set}" ]; then
        # Get the paths, ignoring commented or empty lines
        paths=$(
            grep \
                -v \
                -e '^#' \
                -e '^$' \
                "${PROGDIR}/configs.paths.txt"
            )
    else
        paths=("${SIMPLEBORGWRAPPER_CONFIGDIR}")
    fi

    local config_base_filename="config.sh"
    local config_extra_filenames=("config.local.sh")

    local current_dirpath
    local current_filename
    local current_filepath
    local found_config_dirpath=0

    # Will test each provided paths
    for current_dirpath in ${paths[@]} ; do
        # Will test if the provided path does exists
        if [ -e "${current_dirpath}" -a -d "${current_dirpath}" ]; then
            # Will check for base configuration file in the provided path

            current_filepath="${current_dirpath}/${config_base_filename}"
            if [ -e "${current_filepath}" -a -f "${current_filepath}" ]; then
                source "${current_filepath}"

                # We found the configuration path, we store it in a variable
                found_config_dirpath="${current_dirpath}"

                # Will check for extra configuration files in the found configuration path
                for current_filename in ${config_extra_filenames[@]} ; do
                    # Will test if this extra configuration file exists in the provided path
                    current_filepath="${found_config_dirpath}/${current_filename}"
                    if [ -e "${current_filepath}" -a -f "${current_filepath}" ]; then
                        source "${current_filepath}"
                    fi
                done

                # We found the configuration path, no need to test next provided path
                if [ -n ${found_config_dirpath} ]; then
                    return 0
                fi
            fi
        fi
    done

    # We found no configuration file in any of the provided paths.
    echo_error "Error: [load_configuration()] Could not find configuration files in any of theses paths: $(echo "${paths[@]}" | tr '\n' ' ')" 2
    return 1
}

# /Application functions


# BorgBackup functions

set_passphrase_variable() {
    local password_realfilepath="$(realpath --no-symlinks -- "${REPOSITORIES_PASSWORD_FILEPATH}")"
    if [ ! -e "${password_realfilepath}" ]; then
        (>&2 echo "Error: Passphrase file \"${REPOSITORIES_PASSWORD_FILEPATH}\" is empty.")
        exit 1
    else
        # Make sure there is no remaining of a passphrase somewhere
        export -n BORG_PASSPHRASE

        export BORG_PASSCOMMAND="/bin/cat \"${password_realfilepath}\""
    fi
}

unset_passphrase_variable() {
    export -n BORG_PASSPHRASE
    export -n BORG_PASSCOMMAND
}

set_dirs_variables() {
    export TMPDIR="${BORG_TEMP_DIRPATH}"
    export BORG_CACHE_DIR="${BORG_CACHE_DIRPATH}"
}

unset_dirs_variables() {
    export -n TMPDIR
    export -n BORG_CACHE_DIR
}

##
# Create a new BorgBackup repository.
##
init_repository() {
    if [ -z "${REPOSITORY_DIRPATH}" ] ; then
        echo_error "Error: The path for the repository (\$REPOSITORY_DIRPATH) is empty. Aborting."
        echo_error_and_exit print_usage 2
    fi

    # If folder not existing or existing but empty
    if [ ! -e "${REPOSITORY_DIRPATH}" ] || [ -z "$(\ls -A "${REPOSITORY_DIRPATH}")" ]; then
        set_dirs_variables
        set_passphrase_variable

        echo_info "Initializing a repository at \"${REPOSITORY_DIRPATH}\"..."

        "${BORG_BIN_FILEPATH}" \
            init \
            --encryption="${BORG_ENCRYPTION}" \
            "${REPOSITORY_DIRPATH}"

        unset_passphrase_variable
        unset_dirs_variables
    else
        echo_error_and_exit "Error: The folder for the repository (\"${REPOSITORY_DIRPATH}\") already exists with some data inside. Aborting." 1
    fi
}

##
# Create an archive (a backup) and add it to a BorgBackup repository.
#
# @param archive_name   (Optional) Name of the archive to create. Defaults to
#                       $CREATEARCHIVE_DEFAULT_NAME_PREFIX with current date appended.
##
create_archive() {
    local archive_name

    if [ $# -eq 1 ]; then
        archive_name="${1}" ; shift
    else
        archive_name="${CREATEARCHIVE_DEFAULT_NAME_PREFIX}{utcnow:%Y-%m-%d_%H-%M-%S_%Z}"
    fi

    set_dirs_variables
    set_passphrase_variable

    echo_info "Creating an archive \"${archive_name}\" into repository \"${REPOSITORY_DIRPATH}\"..."

    "${BORG_BIN_FILEPATH}" \
        create \
        --compression "${ARCHIVE_COMPRESSION}" \
        --exclude-from "${CREATEARCHIVE_EXCLUSION_FILEPATH}" \
        --exclude-if-present "${CREATEARCHIVE_SPECIAL_FILE_NAME_TO_EXCLUDE_A_DIRECTORY}" \
        --list \
        "${CREATEARCHIVE_EXTRA_ARGUMENTS[@]}" \
        "${REPOSITORY_DIRPATH}::${archive_name}" \
        "${SOURCE_DIRPATHS[@]}"
        #--stats \

    unset_passphrase_variable
    unset_dirs_variables
}

##
# Give informations about a BorgBackup repository or one of it's archive.
#
# @param archive_name   (Optional) Name of the archive to get information of. If
#                       empty, information about the repository itself will be
#                       listed.
##
repository_infos() {
    local archive_name=""
    local repository_archive_identifier

    if [ $# -ge 1 ]; then
        archive_name="${1}" ; shift
    fi

    if [ -n "${archive_name}" ] ; then
        repository_archive_identifier="${REPOSITORY_DIRPATH}::${archive_name}"
    else
        repository_archive_identifier="${REPOSITORY_DIRPATH}"
    fi

    set_dirs_variables
    set_passphrase_variable

    echo_info "Displaying informations about repository/archive \"${repository_archive_identifier}\"..."

    "${BORG_BIN_FILEPATH}" \
        info \
        "${repository_archive_identifier}"

    unset_passphrase_variable
    unset_dirs_variables
}

##
# List the content of a BorgBackup repository (or of one of it's archive).
#
# It can list:
# * The archives (the backups)
# * The files in a given archive
# * The files in given path(s) of a given archive
#
# @param archive_name   (Optional) Name of the archive to list content of. If
#                       empty, all archives of the repository will be listed.
# @param path...    (Optional) Path(s) (in the archive) to list. If empty, all
#                   paths of the archive will be listed.
##
list_archives() {
    local archive_name=""
    local paths
    local repository_archive_identifier

    if [ $# -ge 1 ]; then
        archive_name="${1}" ; shift
    fi

    paths=("$@")

    if [ -n "${archive_name}" ] ; then
        repository_archive_identifier="${REPOSITORY_DIRPATH}::${archive_name}"
    else
        repository_archive_identifier="${REPOSITORY_DIRPATH}"
    fi

    set_dirs_variables
    set_passphrase_variable

    echo_info "Listing content of repository/archive \"${repository_archive_identifier}\"..."

    "${BORG_BIN_FILEPATH}" \
        list \
        "${repository_archive_identifier}" \
        "${paths[@]}"

    unset_passphrase_variable
    unset_dirs_variables
}

##
# List the file differences between 2 archives of a BorgBackup repository.
#
# It can list the differences between:
# * 2 archives (complete comparison)
# * 2 archives but only for provided path(s)
#
# @param archive_1_name Name of the first archive to compare with.
# @param archive_2_name Name of the second archive to compare with.
# @param path...    (Optional) Path(s) (in the archive(s)) to list. If empty,
#                   all paths of the archive(s) will be considered/used.
##
differences_between_archives() {
    local archive_1_name="${1}" ; shift
    local archive_2_name="${1}" ; shift
    local paths=("$@")
    local repository_archive_identifier

    if [ -z "${archive_1_name}" ] ; then
        echo_error_and_exit "Error: [differences_between_archives()] Invalid argument: archive_1_name is empty." 2
    fi

    if [ -z "${archive_2_name}" ] ; then
        echo_error_and_exit "Error: [differences_between_archives()] Invalid argument: archive_2_name is empty." 2
    fi

    repository_archive_identifier="${REPOSITORY_DIRPATH}::${archive_1_name}"

    set_dirs_variables
    set_passphrase_variable

    echo_info "Computing differences between repository/archive \"${repository_archive_identifier}\" and \"${archive_2_name}\"..."

    "${BORG_BIN_FILEPATH}" \
        diff \
        --sort \
        "${repository_archive_identifier}" \
        "${archive_2_name}" \
        "${paths[@]}"

    unset_passphrase_variable
    unset_dirs_variables
}

##
# Extract data from a BorgBackup archive (which is in a repository).
#
# It can extract:
# * The whole archive.
# * Only given path(s) of the archive
#
# @param destination_path   Path where to restore extracted content to.
# @param archive_name   Name of the archive to extract content from.
# @param path...    (Optional) Path(s) (in the archive) to extract. If empty,
#                   all paths of the archive will be extracted.
##
extract_from_archive() {
    local destination_dirpath="${1}" ; shift
    local archive_name="${1}" ; shift
    local paths=("$@")

    # Create the destination directory
    mkdir --parents "${destination_dirpath}"

    set_dirs_variables
    set_passphrase_variable

    # Move into where we want to extract (have to do it manually because the
    # docs says: "Currently, extract always writes into the current working
    # directory")
    cd "${destination_dirpath}"

    echo_info "Extracting from archive \"${REPOSITORY_DIRPATH}::${archive_name}\"..."

    "${BORG_BIN_FILEPATH}" \
        extract \
        --list \
        "${REPOSITORY_DIRPATH}::${archive_name}" \
        "${paths[@]}"

    # Move back to previous directory
    cd - > /dev/null

    unset_passphrase_variable
    unset_dirs_variables
}

##
# Extract data from a BorgBackup archive (which is in a repository) in a tarball.
#
# It can extract:
# * The whole archive.
# * Only given path(s) of the archive
#
# @param tarball_filepath   Path of the tarball to create (use "-" for stdout).
# @param archive_name   Name of the archive to extract content from.
# @param path...    (Optional) Path(s) (in the archive) to extract. If empty,
#                   all paths of the archive will be extracted.
##
tarball_archive() {
    local tarball_filepath="${1}" ; shift
    local archive_name="${1}" ; shift
    local paths=("$@")

    set_dirs_variables
    set_passphrase_variable

    echo_info "Extracting from archive \"${REPOSITORY_DIRPATH}::${archive_name}\" into \"${tarball_filepath}\"..."

    "${BORG_BIN_FILEPATH}" \
        export-tar \
        --list \
        "${REPOSITORY_DIRPATH}::${archive_name}" \
        "${tarball_filepath}" \
        "${paths[@]}"

    unset_passphrase_variable
    unset_dirs_variables
}

##
# Mount the content of a BorgBackup repository (or of one of it's archive).
#
# It can mount:
# * The whole repository (all archives/backups)
# * A single archive
# * The files in given path(s) of a given archive
#
# @param mount_point    Path on the filesystem where to mount the repo/archive.
# @param archive_name   (Optional) Name of the archive to mount. If empty, all
#                       archives of the repository will be mounted.
# @param path...    (Optional) Path(s) (in the archive) to mount. If set, only
#                   thoses path(s) will be mounted (to other will not appear on
#                   the mount point). If empty all paths of the archive will be
#                   mounted.
##
mount_repository() {
    local mount_point="${1}" ; shift
    local archive_name=""
    local paths
    local repository_archive_identifier

    # Create the mount point
    mkdir --parents "${mount_point}"

    if [ $# -ge 1 ]; then
        archive_name="${1}" ; shift
    fi

    paths=("$@")

    if [ -n "${archive_name}" ] ; then
        repository_archive_identifier="${REPOSITORY_DIRPATH}::${archive_name}"
    else
        repository_archive_identifier="${REPOSITORY_DIRPATH}"
    fi

    set_dirs_variables
    set_passphrase_variable

    echo_info "Mounting repository/archive \"${repository_archive_identifier}\" at \"${mount_point}\"..."

    "${BORG_BIN_FILEPATH}" \
        mount \
        "${repository_archive_identifier}" \
        "${mount_point}" \
        "${paths[@]}"

    unset_passphrase_variable
    unset_dirs_variables
}

##
# Un-mount a BorgBackup repository or archive.
#
# @param mount_point    Path on the filesystem where the repo/archive is mounted.
##
unmount_repository() {
    local mount_point="${1}"

    set_dirs_variables
    set_passphrase_variable

    echo_info "Unmounting repository/archive from \"${mount_point}\"..."

    "${BORG_BIN_FILEPATH}" \
        umount \
        "${mount_point}"

    unset_passphrase_variable
    unset_dirs_variables
}

##
# Remove old data from a BorgBackup repository.
#
# @param archive_name_prefix    (Optional) Limit the prune operation only to
#                               archives whose name start with the provided
#                               prefix.
##
prune_repository() {
    local archive_name_prefix=""
    local extra_arguments=("${PRUNING_EXTRA_ARGUMENTS[@]}")

    if [ $# -ge 1 ]; then
        archive_name_prefix="${1}" ; shift

        if [ -z "${archive_name_prefix}" ] ; then
            echo_error_and_exit "Error: [prune_repository()] Invalid argument: archive_name_prefix is empty." 2
        else
            extra_arguments+=("--prefix" "${archive_name_prefix}")
        fi
    fi

    set_dirs_variables
    set_passphrase_variable

    echo_info "Pruning repository \"${REPOSITORY_DIRPATH}\"..."

    "${BORG_BIN_FILEPATH}" \
        prune \
        --list \
        --stats \
        "${extra_arguments[@]}" \
        "${REPOSITORY_DIRPATH}"

    unset_passphrase_variable
    unset_dirs_variables
}

##
# Remove archive(s) from a BorgBackup repository.
#
# @param archive... (Optional) Archives to delete. If empty, all
#                   archives of the repository will be deleted.
##
delete_archive() {
    local archives=("$@")

    set_dirs_variables
    set_passphrase_variable

    if [ $# -ge 1 ]; then
        echo_info "Deleting $# archive(s) from repository \"${REPOSITORY_DIRPATH}\"..."
    else
        echo_info "Deleting ALL archives from repository \"${REPOSITORY_DIRPATH}\"..."
    fi

    "${BORG_BIN_FILEPATH}" \
        delete \
        --stats \
        "${REPOSITORY_DIRPATH}" \
        "${archives[@]}"

    unset_passphrase_variable
    unset_dirs_variables
}

##
# Check a BorgBackup repository or one of it's archive.
#
# @param archive_name   (Optional) Name of the archive to perform a check on. If
#                       empty, the whole repository will be checked.
##
check_repository() {
    local archive_name=""
    local repository_archive_identifier

    if [ $# -ge 1 ]; then
        archive_name="${1}" ; shift
    fi

    if [ -n "${archive_name}" ] ; then
        repository_archive_identifier="${REPOSITORY_DIRPATH}::${archive_name}"
    else
        repository_archive_identifier="${REPOSITORY_DIRPATH}"
    fi

    set_dirs_variables
    set_passphrase_variable

    echo_info "Checking repository/archive \"${repository_archive_identifier}\"..."

    "${BORG_BIN_FILEPATH}" \
        check \
        --verbose \
        "${repository_archive_identifier}"

    unset_passphrase_variable
    unset_dirs_variables
}

# /BorgBackup functions
