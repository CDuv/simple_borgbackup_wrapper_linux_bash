#!/bin/bash

##
# Create a tarball of a BorgBackup archive.
#
# See print_usage() for how to use.
#
# @author DUVERGIER Claude
##

set -euo pipefail

readonly PROGNAME="$(basename -- "$0")"
readonly PROGDIR="$(readlink -m -- "$(dirname -- "$0")")"
readonly -a ARGS=("$@")


# ####################
# Configuration
# #####

source "${PROGDIR}/functions.sh"
load_configuration

# /Configuration
# ####################


print_usage() {
    echo "Usage: ${PROGNAME} tarball_filepath archive_name [path...]"
}

main() {
    if [ "${#ARGS[@]}" -lt 2 ]; then
        echo_error "Error: Missing argument."
        echo_error_and_exit "$(print_usage)" 2
    else
        tarball_archive "${ARGS[@]}"
    fi
}

main
