#!/bin/bash

##
# Un-mount a mounted BorgBackup repository/archive.
#
# See print_usage() for how to use.
#
# @author DUVERGIER Claude
##

set -euo pipefail

readonly PROGNAME="$(basename -- "$0")"
readonly PROGDIR="$(readlink -m -- "$(dirname -- "$0")")"
readonly -a ARGS=("$@")


# ####################
# Configuration
# #####

source "${PROGDIR}/functions.sh"
load_configuration

# /Configuration
# ####################


print_usage() {
    echo "Usage: ${PROGNAME} mount_point"
}

main() {
    if [ "${#ARGS[@]}" -ne 1 ]; then
        echo_error "Error: Missing argument."
        echo_error_and_exit "$(print_usage)" 2
    else
        unmount_repository "${ARGS[@]}"
    fi
}

main
