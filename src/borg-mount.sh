#!/bin/bash

##
# Mount content of a BorgBackup repository or one of it's archive.
#
# See print_usage() for how to use.
#
# @author DUVERGIER Claude
##

set -euo pipefail

readonly PROGNAME="$(basename -- "$0")"
readonly PROGDIR="$(readlink -m -- "$(dirname -- "$0")")"
readonly -a ARGS=("$@")


# ####################
# Configuration
# #####

source "${PROGDIR}/functions.sh"
load_configuration

# /Configuration
# ####################


print_usage() {
    echo "Usage: ${PROGNAME} mount_point [archive_name] [path...]"
}

main() {
    if [ "${#ARGS[@]}" -lt 1 ]; then
        echo_error "Error: Missing argument."
        echo_error_and_exit "$(print_usage)" 2
    else
        mount_repository "${ARGS[@]}"
    fi
}

main
