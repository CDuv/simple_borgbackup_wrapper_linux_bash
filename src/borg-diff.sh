#!/bin/bash

##
# Compare 2 archive content of a BorgBackup repository.
#
# See print_usage() for how to use.
#
# @author DUVERGIER Claude
##

set -euo pipefail

readonly PROGNAME="$(basename -- "$0")"
readonly PROGDIR="$(readlink -m -- "$(dirname -- "$0")")"
readonly -a ARGS=("$@")


# ####################
# Configuration
# #####

source "${PROGDIR}/functions.sh"
load_configuration

# /Configuration
# ####################


print_usage() {
    echo "Usage: ${PROGNAME} archive_1_name archive_2_name [path...]"
}

main() {
    if [ "${#ARGS[@]}" -lt 2 ]; then
        echo_error "Error: Missing argument."
        echo_error_and_exit "$(print_usage)" 2
    else
        differences_between_archives "${ARGS[@]}"
    fi
}

main
