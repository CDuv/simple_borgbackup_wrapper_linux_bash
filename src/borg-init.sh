#!/bin/bash

##
# Initialize a BorgBackup repository.
#
# See print_usage() for how to use.
#
# @author DUVERGIER Claude
##

set -euo pipefail

readonly PROGNAME="$(basename -- "$0")"
readonly PROGDIR="$(readlink -m -- "$(dirname -- "$0")")"
readonly -a ARGS=("$@")


# ####################
# Configuration
# #####

source "${PROGDIR}/functions.sh"
load_configuration

# /Configuration
# ####################


print_usage() {
    echo "Usage: ${PROGNAME}"
}

main() {
    if [ "${#ARGS[@]}" -ne 0 ]; then
        echo_error "Error: Extra arguments."
        echo_error_and_exit "$(print_usage)" 2
    else
        init_repository "${ARGS[@]}"
    fi
}

main
